﻿using UnityEngine;

public class PickUpCannon : Scrapyard.SYOculusTouchInput
{


	bool GripWasSqueezed;
	public Transform HandTransform;

	public Cannon currentSelectedCannon;
	private bool isPlacing;
	public LayerMask placableLayer;

	Vector2 lastJoystick;
	public float smoothSpeed = 8f;

	public GameObject CannonPrefab;
	public AudioClip dropAudioClip;
	public AudioClip pickupAudioClip;

	[Header("Rotate sound")]
	public int rotateSoundSegments = 32;
	private int lastRotateSegment;

	public AudioClip[] rotateSound;
	public float rotateCooldown = 0.1f;
	float rotateSoundTimer = 0f;
	public float rotateVolume = 0.4f;


	// Update is called once per frame
	void Update()
	{
		base.Update();
		DealWithGripAndUngrip();

		rotateSoundTimer -= Time.deltaTime;

		if (isPlacing)
		{
			MoveAroundCannon();
		}
		else if (currentSelectedCannon != null)
		{
			var joystick = PrimaryThumbstick;
			if (joystick.magnitude > 0.95f)
			{
				var dir = TestPlacer.UltraDirection(HandTransform, currentSelectedCannon.transform.up, joystick);
				currentSelectedCannon.transform.rotation = Quaternion.LookRotation(dir, currentSelectedCannon.transform.up);
				currentSelectedCannon.transform.localRotation *= Quaternion.Euler(0, -90, 0);
				RotateSoundLogic(joystick);
			}
		}

		GameObject rayGO = raycastedGO();
		if (rayGO != null && !isPlacing)
		{
			Cannon cannonLogic = rayGO.GetComponent<Cannon>();
			if (cannonLogic != null)
			{
				bool isNew = cannonLogic != currentSelectedCannon;
				if (isNew)
				{
					if (currentSelectedCannon != null)
					{
						currentSelectedCannon.SetNormalMaterial();
					}
					cannonLogic.SetHighlightedMaterial();
					currentSelectedCannon = cannonLogic;
				}
			}
			else
			{
				if (currentSelectedCannon != null)
				{
					currentSelectedCannon.SetNormalMaterial();
					currentSelectedCannon = null;
				}
			}
		}

		Debug.DrawRay(HandTransform.position, HandTransform.forward);

	}

	void MoveAroundCannon()
	{
		Ray ray = new Ray(HandTransform.position, HandTransform.forward);
		RaycastHit hit;
		if (Physics.Raycast(ray, out hit, 100, placableLayer))
		{
			//if (hit.normal == Vector3.up)
			{
				//currentSelectedCannon.transform.position = hit.point + Vector3.up * 0.02f
				currentSelectedCannon.transform.position = Vector3.Lerp(currentSelectedCannon.transform.position, hit.point, smoothSpeed * Time.deltaTime);

				var joystick = PrimaryThumbstick;
				if (joystick.magnitude > 0.8f)
				{
					lastJoystick = joystick;
					//var direction = GetWorldDirectionFromJoystickDirection(HandTransform, joystick);
					//currentSelectedCannon.RotateTowardsDirection(direction);
				}

				if (lastJoystick.magnitude > 0.01f)
				{
					var dir = TestPlacer.UltraDirection(HandTransform, hit.normal, lastJoystick);
					currentSelectedCannon.transform.rotation = Quaternion.LookRotation(dir, hit.normal);
					currentSelectedCannon.transform.localRotation *= Quaternion.Euler(0, -90, 0);

					RotateSoundLogic(lastJoystick);
				}
				else
				{
					currentSelectedCannon.transform.up = hit.normal;
				}
			}
		}
	}

	void RotateSoundLogic(Vector2 dir, bool play = true)
	{
		if (currentSelectedCannon != null)
		{
			var v = (int)(rotateSoundSegments * (Mathf.Atan2(dir.y, dir.x) + Mathf.PI) / (2f * Mathf.PI));
			if (lastRotateSegment != v && rotateSound.Length > 0 && play)
			{
				if (rotateSoundTimer <= 0)
				{
					AudioClip clip = rotateSound[Random.Range(0, rotateSound.Length)];
					AudioSource.PlayClipAtPoint(clip, currentSelectedCannon.transform.position, rotateVolume);
					rotateSoundTimer = rotateCooldown;
				}
			}
			lastRotateSegment = v;
		}
	}

	void DealWithGripAndUngrip()
	{
		bool gripChanged = GripIsSqueezed != GripWasSqueezed;
		bool aimingAtCannon = currentSelectedCannon != null;

		if (gripChanged && GripIsSqueezed && !aimingAtCannon)
		{
			Debug.Log("Not aiming at cannon!");
			SpawnNewCannon();
			TryToGrabSomething();

		}
		else if (gripChanged && GripIsSqueezed && aimingAtCannon)
		{
			TryToGrabSomething();
		}
		else if (gripChanged && !GripIsSqueezed && currentSelectedCannon != null)
		{
			DropSomething();
		}
		GripWasSqueezed = GripIsSqueezed;
	}

	void SpawnNewCannon()
	{
		GameObject newCannon = Instantiate(CannonPrefab, HandTransform.position, Quaternion.identity);
		currentSelectedCannon = newCannon.GetComponent<Cannon>();
		currentSelectedCannon.VisuallyScaleUp();
		RotateSoundLogic(lastJoystick, false);
	}

	void TryToGrabSomething()
	{
		lastJoystick = Vector2.zero;
		lastJoystick = TestPlacer.InverseUltraDirection(HandTransform, currentSelectedCannon.transform);
		isPlacing = true;
		currentSelectedCannon.SetMovementHappening(isPlacing);
		if (currentSelectedCannon.WasRecentlyCreated())
		{
			currentSelectedCannon.PlaySpawnSound();
		}
		else
		{
			AudioSource.PlayClipAtPoint(pickupAudioClip, currentSelectedCannon.transform.position);
		}
		RotateSoundLogic(lastJoystick, false);
		LaserExtreme.SetLaserTarget(currentSelectedCannon);
	}

	void DropSomething()
	{
		isPlacing = false;
		currentSelectedCannon.SetMovementHappening(isPlacing);
		Vector3 worldPos = currentSelectedCannon.transform.position;
		LaserExtreme.SetLaserTarget(null);

		//if we are not hitting a placeable GO
		//then despawn the thing
		bool raycastHitsNull = raycastedGO() == null;
		if (raycastHitsNull)
		{
			currentSelectedCannon.VisuallyScaleDown();
			Destroy(currentSelectedCannon.gameObject, 1f);
			currentSelectedCannon = null;
		}
		else
		{
			AudioSource.PlayClipAtPoint(dropAudioClip, worldPos);
		}
	}

	private GameObject raycastedGO()
	{
		Ray ray = new Ray(HandTransform.position, HandTransform.forward);
		RaycastHit hit;
		Debug.DrawRay(HandTransform.position, HandTransform.forward);
		GameObject returnGO = null;
		if (Physics.Raycast(ray, out hit, 100))
		{
			returnGO = hit.transform.gameObject;
			Debug.DrawLine(ray.origin, hit.point);

		}
		return returnGO;
	}

	Vector3 GetWorldDirectionFromJoystickDirection(Transform hand, Vector2 joystick)
	{
		var hf = hand.forward;
		hf.y = 0f;
		var hr = new Vector3(hf.z, 0, -hf.x);

		return joystick.x * hr + joystick.y * hf;
	}
}
