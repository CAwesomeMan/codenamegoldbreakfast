﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Scrapyard {
    public enum ControllerHand {
        Left = 0,
        Right = 1
    }

    public class SYOculusTouchInput : MonoBehaviour {

        public ControllerHand ThisHand;
        private OVRInput.Controller ThisController;

        public bool ThumbIsNearButtonsOrThumbrest { get; private set; }
        public bool IndexFingerIsPointing { get; private set; }
        public bool GripIsSqueezed { get; private set; }

        public bool NothingIsTouching { get; private set; }

        public bool TriggerIsSqueezed { get; private set; }
        public bool BothButtonsArePressed { get; private set; }

        public bool ButtonOneIsPressed { get; private set; }
        public bool ButtonOneWasJustPressed { get; private set; }

        public bool ButtonTwoIsPressed { get; private set; }
        public bool ButtonTwoWasJustPressed { get; private set; }

		public Vector2 PrimaryThumbstick { get; private set; }

		protected bool IsPointingWithIndexFinger { get { return ThumbIsNearButtonsOrThumbrest && IndexFingerIsPointing && GripIsSqueezed; } }
        protected bool IsDoingFlatHand { get { return NothingIsTouching && !GripIsSqueezed; } }
        protected bool IsSqueezingController { get { return GripIsSqueezed && TriggerIsSqueezed && BothButtonsArePressed; } }

        public void Awake() {
            if (ThisHand == ControllerHand.Left) {
                ThisController = OVRInput.Controller.LTouch;
            } else {
                ThisController = OVRInput.Controller.RTouch;
            }
        }

        //public void Update() {
        //    IndexFingerIsPointing = OVRInput.Get(OVRInput.NearTouch.PrimaryIndexTrigger, ThisController) == false;
        //}

        // Update is called once per frame
        public void Update() {
            IndexFingerIsPointing = OVRInput.Get(OVRInput.NearTouch.PrimaryIndexTrigger, ThisController) == false;
            ThumbIsNearButtonsOrThumbrest = OVRInput.Get(OVRInput.Touch.PrimaryThumbRest, ThisController) || OVRInput.Get(OVRInput.NearTouch.PrimaryThumbButtons, ThisController);
            GripIsSqueezed = OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, ThisController) > 0;
            NothingIsTouching = OVRInput.Get(OVRInput.Touch.Any, ThisController) == false && OVRInput.Get(OVRInput.Touch.Any, ThisController) == false;
            TriggerIsSqueezed = OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger, ThisController) > 0;
            BothButtonsArePressed = OVRInput.Get(OVRInput.Touch.One, ThisController) && OVRInput.Get(OVRInput.Touch.Two, ThisController);

            bool buttonOneWasPressedLastFrame = ButtonOneIsPressed;
            ButtonOneIsPressed = OVRInput.Get(OVRInput.Button.One, ThisController);
            ButtonOneWasJustPressed = ButtonOneIsPressed && !buttonOneWasPressedLastFrame;

            bool buttonTwoWasPressedLastFrame = ButtonTwoIsPressed;
            ButtonTwoIsPressed = OVRInput.Get(OVRInput.Button.Two, ThisController);
            ButtonTwoWasJustPressed = ButtonTwoIsPressed && !buttonTwoWasPressedLastFrame;

			PrimaryThumbstick = OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick, ThisController);
			//Debug.Log(OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger, ThisController));
			//if (OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger, ThisController) > 0) {
			//    string hand = ThisHand == ControllerHand.Right ? " right " : " left ";
			//    Debug.Log(hand + " NEW");
			//    Debug.Log("neartouch trigger prim: " + OVRInput.Get(OVRInput.NearTouch.PrimaryIndexTrigger, ThisController));
			//    Debug.Log("pressed trigger prim: " + OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger, ThisController));
			//    Debug.Log("pressed trigger grip: " + OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, ThisController));
			//    Debug.Log("neartouch button 1 or 2: " + OVRInput.Get(OVRInput.NearTouch.PrimaryThumbButtons, ThisController));
			//    Debug.Log("pressed button1: " + OVRInput.Get(OVRInput.Button.One, ThisController));
			//    Debug.Log("pressed button2: " + OVRInput.Get(OVRInput.Button.Two, ThisController));
			//    Debug.Log("TOUCH: prim trigger: " + OVRInput.Get(OVRInput.Touch.PrimaryIndexTrigger, ThisController));
			//    Debug.Log("TOUCH: button1 " + OVRInput.Get(OVRInput.Touch.One, ThisController));
			//    Debug.Log("TOUCH: button2 " + OVRInput.Get(OVRInput.Touch.Two, ThisController));
			//    Debug.Log("TOUCH: thumbrest: " + OVRInput.Get(OVRInput.Touch.PrimaryThumbRest, ThisController));
			//}
			//}
			//              None = 0,                            ///< Maps to RawTouch: [Gamepad, Touch, LTouch, RTouch, LTrackedRemote, RTrackedRemote, Touchpad, Remote: None]
			//One = Button.One,                   ///< Maps to RawTouch: [Touch, RTouch: A], [LTouch: X], [Gamepad, LTrackedRemote, RTrackedRemote, Touchpad, Remote: None]
			//Two = Button.Two,                   ///< Maps to RawTouch: [Touch, RTouch: B], [LTouch: Y], [Gamepad, LTrackedRemote, RTrackedRemote, Touchpad, Remote: None]
			//Three = Button.Three,                 ///< Maps to RawTouch: [Touch: X], [Gamepad, LTouch, RTouch, LTrackedRemote, RTrackedRemote, Touchpad, Remote: None]
			//Four = Button.Four,                  ///< Maps to RawTouch: [Touch: Y], [Gamepad, LTouch, RTouch, LTrackedRemote, RTrackedRemote, Touchpad, Remote: None]
			//PrimaryIndexTrigger = Button.PrimaryIndexTrigger,   ///< Maps to RawTouch: [Touch, LTouch: LIndexTrigger], [RTouch: RIndexTrigger], [Gamepad, LTrackedRemote, RTrackedRemote, Touchpad, Remote: None]
			//PrimaryThumbstick = Button.PrimaryThumbstick,     ///< Maps to RawTouch: [Touch, LTouch: LThumbstick], [RTouch: RThumbstick], [Gamepad, LTrackedRemote, RTrackedRemote, Touchpad, Remote: None]
			//PrimaryThumbRest = 0x00001000,                   ///< Maps to RawTouch: [Touch, LTouch: LThumbRest], [RTouch: RThumbRest], [Gamepad, LTrackedRemote, RTrackedRemote, Touchpad, Remote: None]
			//PrimaryTouchpad = Button.PrimaryTouchpad,       ///< Maps to RawTouch: [LTrackedRemote, Touchpad: LTouchpad], [RTrackedRemote: RTouchpad], [Gamepad, Touch, LTouch, RTouch, Remote: None]
			//SecondaryIndexTrigger = Button.SecondaryIndexTrigger, ///< Maps to RawTouch: [Touch: RIndexTrigger], [Gamepad, LTouch, RTouch, LTrackedRemote, RTrackedRemote, Touchpad, Remote: None]
			//SecondaryThumbstick = Button.SecondaryThumbstick,   ///< Maps to RawTouch: [Touch: RThumbstick], [Gamepad, LTouch, RTouch, LTrackedRemote, RTrackedRemote, Touchpad, Remote: None]
			//SecondaryThumbRest = 0x00100000,                   ///< Maps to RawTouch: [Touch: RThumbRest], [Gamepad, LTouch, RTouch, LTrackedRemote, RTrackedRemote, Touchpad, Remote: None]
			//SecondaryTouchpad = Button.SecondaryTouchpad,     ///< Maps to RawTouch: [Gamepad, Touch, LTouch, RTouch, LTrackedRemote, RTrackedRemote, Touchpad, Remote: None]
			//Any = ~None,        
		}
    }
}