﻿using UnityEngine;

public class TestPlacer : MonoBehaviour
{
	public Cannon target;

	public Vector3 pos;
	public Vector3 normal;

	Vector2 joystick;

	// Start is called before the first frame update
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{
		RaycastHit hit;
		Ray ray = new Ray(transform.position, transform.forward);
		if (Physics.Raycast(ray, out hit, 1000f, LayerMask.GetMask("PlacableLayer")))
		{
			pos = hit.point;
			normal = hit.normal;

			var dir = UltraDirection(transform, normal, joystick);

			target.transform.position = pos;
			target.transform.rotation = Quaternion.LookRotation(dir, normal);
		}
		else
		{
			pos = transform.position;
		}

		if (Input.GetKey(KeyCode.RightArrow))
			joystick.x += 1f;
		if (Input.GetKey(KeyCode.UpArrow))
			joystick.y += 1f;
		if (Input.GetKey(KeyCode.LeftArrow))
			joystick.x -= 1f;
		if (Input.GetKey(KeyCode.DownArrow))
			joystick.y -= 1f;

		joystick.Normalize();


	}

	static Vector3 Proj(Vector3 normal, Vector3 vec)
	{
		return (vec - normal * Vector3.Dot(normal, vec)).normalized;
	}

	private void OnDrawGizmos()
	{
		Gizmos.color = Color.blue;
		Gizmos.DrawWireSphere(transform.position, 0.05f);
		Gizmos.DrawLine(transform.position, transform.forward * 100);

		Gizmos.color = Color.red;
		Gizmos.DrawLine(transform.position, transform.position + transform.right * 0.2f);
		Gizmos.color = Color.green;
		Gizmos.DrawLine(transform.position, transform.position + transform.up * 0.2f);

		var right = Proj(normal, transform.right);
		var up = Proj(normal, transform.up);

		Gizmos.color = Color.red;
		Gizmos.DrawLine(pos, pos + right * 0.4f);
		Gizmos.color = Color.green;
		Gizmos.DrawLine(pos, pos + up * 0.4f);

		//var dir = right * joystick.x + up * joystick.y;
		var dir = UltraDirection(transform, normal, joystick);
		Gizmos.color = Color.blue;
		Gizmos.DrawLine(pos, pos + dir * 0.4f);

		Gizmos.color = Color.green;
		Gizmos.DrawWireSphere(pos, 0.05f);
		//	Gizmos.DrawLine(pos, pos + normal * 0.6f);
	}

	public static Vector3 UltraDirection(Transform hand, Vector3 normal, Vector2 joystick)
	{
		var right = Proj(normal, hand.right);
		var up = Proj(normal, hand.up);

		var dir = right * joystick.x + up * joystick.y;
		return dir;
	}

	public static Vector2 InverseUltraDirection(Transform hand, Transform cannon)
	{
		var right = Vector3.Dot(hand.right, cannon.forward);
		var up = Vector3.Dot(hand.up, cannon.forward);

		return new Vector2(right, up).normalized;
	}
}
