﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallShooterInput : Scrapyard.SYOculusTouchInput {
    public BallSpawner ballSpawner;
    // Update is called once per frame
    void Update() {
        base.Update();
        if (ButtonOneWasJustPressed) {
            Debug.Log("Want to spawn ball with oculus touchhh!");
            ballSpawner.SpawnBall();
        }
    }
}
