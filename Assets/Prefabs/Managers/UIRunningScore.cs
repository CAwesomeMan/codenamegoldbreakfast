﻿using UnityEngine;
using System.Collections;
using TMPro;
using UnityEngine.SceneManagement;

public class UIRunningScore : MonoBehaviour {

    private string _TextToDisplay;
    private TextMeshPro _ScoreTextMesh;
    private bool _UIShouldBeActive = false;
    private GameManagerInterface _CurrentInterface;
    private float lastScore;

    public static UIRunningScore _instance;

	

    // Use this for initialization
    void Awake() {
        _instance = this;
        _ScoreTextMesh = GetComponent<TextMeshPro>();
        Debug.Assert(_ScoreTextMesh != null, "Needs text mesh for " + this.GetType().Name);

        _CurrentInterface = Object.FindObjectOfType<GameManagerInterface>();
    }

    void OnEnable() {
        GameManagerInterface.OnScoreChange += UpdateScore;
        GameManagerInterface.OnGameStop += GameEnded;
        GameManagerInterface.OnGameStart += GameStarted;
    }

    void OnDisable() {
        GameManagerInterface.OnScoreChange -= UpdateScore;
        GameManagerInterface.OnGameStop -= GameEnded;
        GameManagerInterface.OnGameStart -= GameStarted;

    }

    void GameStarted() {
        _UIShouldBeActive = true;
    }

    void GameEnded( float finalTime, float finalScore ) {
		SceneManager.LoadScene(1);
        string gameEnded = "CONGRATULATIONS";
        //gameEnded += "\nFinal Score: " + finalScore.ToString("F0") + "p";
        gameEnded += "\n" + finalScore.ToString("F0") + "coffee mugs";
        _ScoreTextMesh.text = gameEnded;
        _UIShouldBeActive = false;
    }

    void UpdateScore( float NewScore ) {
        lastScore = NewScore;
    }

    void Update() {
        if (_UIShouldBeActive) {
            _TextToDisplay = TextBasedOnRunningGameTime();
            _ScoreTextMesh.text = _TextToDisplay;
        }

    }

    string TextBasedOnRunningGameTime() {
        float currentTime = _CurrentInterface.CurrentGameTime;
        float minutes = ((int)currentTime) / 60;
        float seconds = currentTime - minutes * 60f;
        string formattedTime = seconds.ToString("0.0");
        if (minutes > 0) {
            formattedTime = minutes.ToString("0.") + ":" + formattedTime;
        }
        formattedTime = formattedTime + "\n" + lastScore.ToString("0.0") + " cups";
        return formattedTime;

    }

    public void ForceUI( string uiText ) {
        _ScoreTextMesh.text = uiText;
    }
}
