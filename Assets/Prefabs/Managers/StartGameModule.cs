﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartGameModule : MonoBehaviour {

    public float Countdown = 3f;
    // Start is called before the first frame update
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        if (Time.time > Countdown && !GameManagerInterface._instance.GameIsRunning) {
            GameManagerInterface._instance.StartGame();
            Destroy(this);
        } else {
            float secLeft = Countdown - Time.time;
            string msg = "-" + secLeft.ToString("0.0") + " sec";
            UIRunningScore._instance.ForceUI(msg);
        }
    }
}
