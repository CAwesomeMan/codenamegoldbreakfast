﻿using UnityEngine;
using System.Collections;

public class GameManagerInterface : MonoBehaviour {

    public static GameManagerInterface _instance;

    public delegate void GameStarted();

    public static event GameStarted OnGameStart;

    public delegate void GameStopped( float finalTime, float finalScore );

    public static event GameStopped OnGameStop;

    public delegate void ScoreChanged( float newScore );

    public static event ScoreChanged OnScoreChange;

    public float GameLength = 60 * 3;

    private float _GameScore;
    private float _StartGameTime;
    private bool _GameIsRunning;

    private void Awake() {
        _instance = this;
    }

    public bool GameIsRunning {
        get {
            return _GameIsRunning;
        }
    }

    public float CurrentGameTime {
        get {
            return GameLength - (Time.time - _StartGameTime);
        }
    }

    private void Update() {
        if (CurrentGameTime < 0) {
            StopGame();
        }
    }

    public void StartGame() {
        _StartGameTime = Time.time;
        _GameScore = 0;
        _GameIsRunning = true;
        if (OnGameStart != null) {
            OnGameStart();
        }
    }

    public void AddScore( float Score ) {
        _GameScore += Score;
        if (OnScoreChange != null) {
            OnScoreChange(_GameScore);
        }
    }

    public void StopGame() {
        _GameIsRunning = false;
        float finalTime = CurrentGameTime;
        float finalScore = _GameScore;
        if (OnGameStop != null) {
            OnGameStop(finalTime, finalScore);
        }
    }
}