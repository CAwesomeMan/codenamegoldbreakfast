﻿using UnityEngine;

public class LaserExtreme : MonoBehaviour
{
	public Vector3 pos;
	public Vector3 vel;
	public LineRenderer lineRenderer;

	public Cannon target;

	public static void SetLaserTarget(Cannon cannon)
	{
		if (instance != null)
		{
			instance.target = cannon;
		}
	}

	static LaserExtreme instance;

	void Start()
	{
		instance = this;
	}

	// Update is called once per frame
	void Update()
	{
		if (target == null)
		{
			TurnOff();
		}
		else
		{
			Set(target.spawnTransform.position, target.spawnTransform.forward * target.power);
		}
	}

	public void Set(Vector3 pos, Vector3 vel)
	{
		Vector3 prev = pos;

		for (int i = 0; i < lineRenderer.positionCount; i++)
		{
			vel += Physics.gravity * Time.fixedDeltaTime;
			pos += vel * Time.fixedDeltaTime;

			lineRenderer.SetPosition(i, pos);

			prev = pos;
		}

		lineRenderer.gameObject.SetActive(true);
	}

	public void TurnOff()
	{
		lineRenderer.gameObject.SetActive(false);
	}
}
