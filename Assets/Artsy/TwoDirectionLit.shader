﻿Shader "Scrapyard/Lit/SingleDirectionSineColor" {
	//taken from http://krazydad.com/tutorials/makecolors.php
	Properties{
		_PulseFrequency("Pulse Frequency", Vector) = (3,3,3)
		_PhaseShift("Phase Shift", Vector) = (0,2.1,4.2,0) //2.1 shift between each is standard
		_ShiftDirection("ShiftDirection", Vector) = (0, -1, 0, 0) // (x, y, z, w)

		_RGBWaveModifier("RGBModifier", Vector) = (1,1,1) // 1 is standard
		 _RGBBaseLine("RGBBaseline", Vector) = (0,0,0) // 0 is standard

		_ColorWidth("Color Width", Range(-3, 3)) = 0.5
		_ColorCenter("Color Center", Range(-3, 3)) = 0.5

		_UseWorldPosAsPhaseShift("UseWorldPosAsPhaseShift", Range(0, 1)) = 0

	}
		SubShader{
				Tags { "RenderType" = "Opaque" }
				CGPROGRAM
				#pragma surface surf Lambert vertex:vert

				float4 _PulseFrequency;
				float4 _PhaseShift;
				float4 _ShiftDirection;
				float4 _RGBWaveModifier;
				float4 _RGBBaseline;
				float _ColorCenter;
				float _ColorWidth;
				float _UseWorldPosAsPhaseShift;

				struct Input {
					float3 worldPos;
					float3 objPos;
				};

				void vert(inout appdata_full v, out Input o) {
					UNITY_INITIALIZE_OUTPUT(Input, o);
					o.objPos = v.vertex.xyz;
				}

				half3 colorRightNow(float3 objPos, float3 worldPos)
				{
					float time = _Time.y;
					float startPhaseShift = (_ShiftDirection.x * objPos.x + _ShiftDirection.y * objPos.y + _ShiftDirection.z * objPos.z) * (1 - _UseWorldPosAsPhaseShift)
						+ (_ShiftDirection.x * worldPos.x + _ShiftDirection.y * worldPos.y + _ShiftDirection.z * worldPos.z) * _UseWorldPosAsPhaseShift;
					half3 colorrr = half3(sin(_PulseFrequency.x * time + _PhaseShift.x + startPhaseShift) * _RGBWaveModifier.x + _RGBBaseline.x,
											sin(_PulseFrequency.y * time + _PhaseShift.y + startPhaseShift) * _RGBWaveModifier.y + _RGBBaseline.y,
											sin(_PulseFrequency.z * time + _PhaseShift.z + startPhaseShift) * _RGBWaveModifier.z + _RGBBaseline.z) * _ColorWidth + _ColorCenter;
					return colorrr;
				}

				void surf(Input IN, inout SurfaceOutput o) {
						o.Albedo = colorRightNow(IN.objPos,IN.worldPos);
				}
				ENDCG
	}
		Fallback "Diffuse"
}