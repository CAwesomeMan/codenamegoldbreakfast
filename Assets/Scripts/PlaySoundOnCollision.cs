﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySoundOnCollision : MonoBehaviour
{
	public AudioClip[] collisionSound;

	private void OnCollisionEnter(Collision collision)
	{
		AudioClip clip = collisionSound[Random.Range(0, collisionSound.Length)];
		AudioSource.PlayClipAtPoint(clip, this.transform.position);
	}
}
