﻿using System.Collections;
using UnityEngine;

public class GoalTarget : MonoBehaviour
{
	bool hasBeenTriggered = false;

	private void OnTriggerEnter(Collider other)
	{
		var ball = other.GetComponent<TennisBall>();
		if (ball != null)
		{
			TriggerGoal(ball);
		}
	}

	public virtual IEnumerator PlayTriggerSequence(TennisBall ball)
	{
		yield break;
	}

	public void TriggerGoal(TennisBall ball)
	{
		if (!hasBeenTriggered && !ball.IsLocked)
		{
			hasBeenTriggered = true;
			//Do stuff
			Debug.Log("Good job man!");
			StartCoroutine(PlayTriggerSequence(ball));
		}
	}
}
