﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxGoalTarget : GoalTarget
{
	public Transform root;
	public float boomDuration = 0.4f;

	public override IEnumerator PlayTriggerSequence(TennisBall ball)
	{
		float t = 0f;
		while (t < 1f)
		{
			t += Time.deltaTime / boomDuration;
			yield return null;

			root.localScale = Vector3.one * Mathf.Clamp01(1 - t);
		}

		Destroy(root.gameObject);
	}
}
