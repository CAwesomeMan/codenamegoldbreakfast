﻿using UnityEngine;

public class Collector : BallConsumer
{
	public override void ConsumeBall(TennisBall ball)
	{
		GameManagerInterface._instance.AddScore(1.0f);
		Destroy(ball.gameObject);
	}
}
