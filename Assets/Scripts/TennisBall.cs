using System;
using UnityEngine;

public class TennisBall : MonoBehaviour
{
	public bool IsLocked { get; set; } = false;
	public float timeout = 10f;
	public GameObject deathEffectPrefab;

	float deathTimer = 0f;

	public Rigidbody Body { get; set; }
	// Start is called before the first frame update
	void Awake()
	{
		Body = GetComponent<Rigidbody>();
	}

	// Update is called once per frame
	void Update()
	{
		deathTimer += Time.deltaTime;
		if (deathTimer > timeout)
		{
			KillMe();
		}
	}

	private void OnCollisionEnter(Collision collision)
	{
		deathTimer = 0f;
	}

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.tag == "NoDeath")
        {
            deathTimer = 0f;
        }
    }

	private void OnTriggerEnter(Collider other)
	{
		deathTimer = 0f;
	}

	private void KillMe()
	{
		if (deathEffectPrefab != null)
		{
			Instantiate(deathEffectPrefab, transform.position, Quaternion.identity);
		}

		Destroy(this.gameObject);
	}
}
