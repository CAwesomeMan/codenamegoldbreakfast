﻿using UnityEngine;

public class BallConsumeTrigger : MonoBehaviour
{
	[SerializeField]
	BallConsumer ballConsumer;

	public void OnTriggerEnter(Collider other)
	{
		var ball = other.GetComponent<TennisBall>();
		if (ball != null && !ball.IsLocked)
		{
			ballConsumer.ConsumeBall(ball);
		}
	}
}
