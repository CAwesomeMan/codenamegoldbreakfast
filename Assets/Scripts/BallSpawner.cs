﻿using UnityEngine;

public class BallSpawner : MonoBehaviour {
    public Transform ballSpawnPoint;
    public TennisBall ballPrefab;

    void Update() {
        if (Input.GetKeyDown(KeyCode.B)) {
            SpawnBall();
        }
    }

    public void SpawnBall() {
        if (ballSpawnPoint != null) {
            Instantiate(ballPrefab, ballSpawnPoint.position, Quaternion.identity);
        } else {
            Instantiate(ballPrefab, transform.position, Quaternion.identity);
        }
    }

    private void OnDrawGizmos() {
        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(transform.position, 0.02f);
        Gizmos.DrawLine(transform.position, transform.position + Vector3.down * 10);
    }
}
