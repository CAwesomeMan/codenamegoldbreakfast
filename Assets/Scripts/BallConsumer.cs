﻿using UnityEngine;

public abstract class BallConsumer : MonoBehaviour
{
	public abstract void ConsumeBall(TennisBall ball);
}
