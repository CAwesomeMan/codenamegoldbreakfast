﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiCannon : BallConsumer
{
	[SerializeField]
	List<Transform> spawnTransforms = new List<Transform>();
	[SerializeField]
	TennisBall ballPrefab;
	[SerializeField]
	float power = 20f;

	public override void ConsumeBall(TennisBall ball)
	{
		StartCoroutine(ProcessBallSequence(ball));
	}

	IEnumerator ProcessBallSequence(TennisBall ball)
	{
		ball.IsLocked = true;
		ball.Body.velocity = Vector3.zero;
		ball.Body.angularVelocity = Vector3.zero;
		ball.gameObject.SetActive(false);

		yield return new WaitForSeconds(1f);

		ball.IsLocked = false;

		for (int i = 0; i < spawnTransforms.Count; i++)
		{
			var spawn = spawnTransforms[i];

			if (i != 0)
			{
				ball = Instantiate(ballPrefab, spawn.position, Quaternion.identity);
			}

			ball.transform.position = spawn.position;
			ball.Body.velocity = spawn.forward * power;
			ball.gameObject.SetActive(true);
		}
	}

	private void OnDrawGizmosSelected()
	{
		foreach (var spawn in spawnTransforms)
		{
			AimHelpGizmo(spawn, power);
		}
	}

	public static void AimHelpGizmo(Transform root, float power)
	{
		if (root == null)
			return;

		Vector3 pos = root.position;
		Vector3 prev = pos;
		Vector3 vel = root.forward * power;

		Gizmos.color = Color.red;

		for (int i = 0; i < 100; i++)
		{
			vel += Physics.gravity * Time.fixedDeltaTime;
			pos += vel * Time.fixedDeltaTime;

			Gizmos.DrawLine(pos, prev);

			prev = pos;
		}
	}
}
