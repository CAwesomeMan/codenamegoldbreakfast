﻿using UnityEngine;

public class DestroyAfterTime : MonoBehaviour
{
	public float time = 5f;

	// Start is called before the first frame update
	void Start()
	{
		Destroy(this.gameObject, time);
	}
}
