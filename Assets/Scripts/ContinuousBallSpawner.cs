﻿using System;
using UnityEngine;

public class ContinuousBallSpawner : MonoBehaviour
{
	public TennisBall ballPrefab;
	public float power;
	public float interval;
	public Transform spawnRoot;

	float timer = 0f;

	// Start is called before the first frame update
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{
		timer += Time.deltaTime;
		if (timer > interval)
		{
			timer = 0f;
			Spawn();
		}
	}

	private void OnDrawGizmos()
	{
		Cannon.AimHelpGizmo(spawnRoot, power);
	}

	private void Spawn()
	{
		var ball = Instantiate(ballPrefab, transform.position, Quaternion.identity);
		ball.Body.velocity = spawnRoot.forward * power;
	}
}
