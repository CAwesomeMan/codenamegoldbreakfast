﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Cannon : BallConsumer
{
	[SerializeField]
	public Transform spawnTransform;
	[SerializeField]
	public float power = 20f;

	public Material highLightMat;
	public Material placingMat;
	private Material normalMat;

	private List<Renderer> childrenRends;
	List<Collider> colliders;
	Rigidbody body;

	public AudioClip shootingSound;
	public AudioClip windUpSound;
	public Transform lerpTarget;
	private AudioSource spawnAudioSource;

	bool isScaling;

	private void Awake()
	{
		childrenRends = GetComponentsInChildren<Renderer>().ToList();
		colliders = GetComponentsInChildren<Collider>().ToList();
		normalMat = childrenRends[0].material;
		body = GetComponentInChildren<Rigidbody>();
		spawnAudioSource = GetComponent<AudioSource>();
	}

	public bool WasRecentlyCreated()
	{
		return isScaling;
	}

	public override void ConsumeBall(TennisBall ball)
	{
		StartCoroutine(ProcessBallSequence(ball));
	}

	IEnumerator ProcessBallSequence(TennisBall ball)
	{
		AudioSource.PlayClipAtPoint(windUpSound, this.transform.position);
		ball.IsLocked = true;
		ball.Body.velocity = Vector3.zero;
		ball.Body.angularVelocity = Vector3.zero;
		ball.Body.isKinematic = true;

		var start = ball.transform.position;
		float t = 0.0f;
		while (t < 1f)
		{
			t += Time.deltaTime / 0.2f;
			ball.transform.position = Vector3.Lerp(start, lerpTarget.transform.position, t);
			yield return null;
		}
		//ball.gameObject.SetActive(false);

		yield return new WaitForSeconds(0.8f);
		AudioSource.PlayClipAtPoint(shootingSound, this.transform.position);
		ball.transform.position = spawnTransform.position;
		ball.Body.isKinematic = false;
		ball.Body.velocity = spawnTransform.forward * power;
		//ball.gameObject.SetActive(true);


		ball.IsLocked = false;
	}

	private void OnDrawGizmosSelected()
	{
		AimHelpGizmo(spawnTransform, power);
	}

	public void RotateTowardsDirection(Vector3 dir)
	{
		dir.y = 0f;
		transform.right = dir.normalized;
	}

	public static void AimHelpGizmo(Transform root, float power)
	{
		if (root == null)
		{
			return;
		}

		Vector3 pos = root.position;
		Vector3 prev = pos;
		Vector3 vel = root.forward * power;

		Gizmos.color = Color.red;

		for (int i = 0; i < 100; i++)
		{
			vel += Physics.gravity * Time.fixedDeltaTime;
			pos += vel * Time.fixedDeltaTime;

			Gizmos.DrawLine(pos, prev);

			prev = pos;
		}
	}

	public void SetMovementHappening(bool enabled)
	{
		//body.isKinematic = enabled;

		if (enabled)
		{
			SetPlacingdMaterial();
		}
		else
		{
			SetHighlightedMaterial();
		}

		foreach (var collider in colliders)
		{
			if (!collider.isTrigger)
			{
				collider.enabled = !enabled;
			}
		}
	}

	public void SetPlacingdMaterial()
	{
		foreach (Renderer rend in childrenRends)
		{
			rend.material = placingMat;
		}
	}

	public void SetHighlightedMaterial()
	{
		foreach (Renderer rend in childrenRends)
		{
			rend.material = highLightMat;
		}
	}

	public void SetNormalMaterial()
	{
		foreach (Renderer rend in childrenRends)
		{
			rend.material = normalMat;
		}
	}

	public void VisuallyScaleUp()
	{
		Vector3 endScale = transform.localScale;
		Vector3 startScale = Vector3.zero;
		StartCoroutine(VisuallyScaleOverTime(startScale, endScale, 0.3f));
	}

	public void VisuallyScaleDown()
	{
		Vector3 startScale = transform.localScale;
		Vector3 endScale = Vector3.zero;
		StartCoroutine(VisuallyScaleOverTime(startScale, endScale, 0.3f));
	}

	private IEnumerator VisuallyScaleOverTime(Vector3 startScale, Vector3 endScale, float duration)
	{

		Transform thisTransform = transform;
		float progress = 0;
		isScaling = true;
		while (progress < 1)
		{
			progress = Mathf.Clamp01(progress + Time.deltaTime / duration);
			Vector3 scaleThisFrame = Vector3.Lerp(startScale, endScale, progress);
			thisTransform.localScale = scaleThisFrame;
			yield return null;
		}
		isScaling = false;
	}

	public void PlaySpawnSound()
	{
		spawnAudioSource.Play();
	}
}
